using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;
using System.IO;

public class ClickSpacePoint : MonoBehaviour
{
   public TMPro.TextMeshProUGUI output;
   private int maxLength = 20;
   public void MyClickSpace()
   {
     AddedSymb("-");
   }
   public void MyClickPoint()
   {
     AddedSymb("·");
   }
   private void AddedSymb(string symb)
   {
     if(output.text.Length < maxLength)
      output.text += symb;
   }
   public void DeleteLastChar()
   {
      if(output.text.Length > 0)
        output.text = output.text.Substring(0, output.text.Length - 1);
   }
}
