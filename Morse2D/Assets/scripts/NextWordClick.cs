using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;
using System.IO;

public class NextWordClick : MonoBehaviour
{
   public TMPro.TextMeshProUGUI output;
   private string path = "Assets\\Resources\\words.txt";
   private string[] words;
   private List<string> usedWords;
   // Start is called before the first frame update
   void Start()
   {
      words = File.ReadAllLines(path);
      string txt = GetRandomWord();
      output.text = txt;
      usedWords = new List<string>(words);
      usedWords.Remove(txt);
   }

   // Update is called once per frame
   void Update()
   {

   }
   public void ReadStringInput()
   {
      if(usedWords.Count == 0)
        usedWords = new List<string>(words);
      string txt = GetRandomWord();
      output.text = txt;
      usedWords.Remove(txt);
      Debug.Log(txt);
   }
   public string GetRandomWord()
   {
      int random = Convert.ToInt32(UnityEngine.Random.Range(0, words.Length));
      return words[random];
   }
}
