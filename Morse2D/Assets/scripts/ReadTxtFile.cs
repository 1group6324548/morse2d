using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ReadTxtFile : MonoBehaviour
{
  public Button targetButton; // кнопка, цвет которой мы хотим 
  private Button changeColor;
  string newText;
    void Start()
    {
        newText = Application.dataPath + "/words.txt";
        Button[] buttons = FindObjectsOfType<Button>(); // Получаем все кнопки на сцене

        foreach (Button button in buttons)
        {
            button.onClick.AddListener(() => ChangeColor(button));
        }
    }

    void ChangeColor(Button btn)
    {
      changeColor = btn;
    }
  private void Awake()
  {
    DontDestroyOnLoad(this.gameObject);
  }
    public void ХText()
  {
    File.WriteAllText(newText, "Х.txt");
  }
    public void СText()
  {
    File.WriteAllText(newText, "С.txt");
  }
    public void ИText()
  {
    File.WriteAllText(newText, "И" + ".txt");
  }
    public void ЕText()
  {
    File.WriteAllText(newText, "Е" + ".txt");
  }
    public void ТText()
  {
    File.WriteAllText(newText, "Т" + ".txt");
  }
    public void МText()
  {
    File.WriteAllText(newText, "М" + ".txt");
  }
    public void ОText()
  {
    File.WriteAllText(newText, "О" + ".txt");
  }
    public void ШText()
  {
    File.WriteAllText(newText, "Ш" + ".txt");
  }
    public void ЖText()
  {
    File.WriteAllText(newText, "Ж" + ".txt");
  }
    public void УText()
  {
    File.WriteAllText(newText, "У" + ".txt");
  }
    public void АText()
  {
    File.WriteAllText(newText, "А" + ".txt");
  }
    public void НText()
  {
    File.WriteAllText(newText, "Н" + ".txt");
  }
    public void ГText()
  {
    File.WriteAllText(newText, "Г" + ".txt");
  }
    public void ЧText()
  {
    File.WriteAllText(newText, "Ч" + ".txt");
  }
    public void ЭText()
  {
    File.WriteAllText(newText, "Э" + ".txt");
  }
    public void ФText()
  {
    File.WriteAllText(newText, "Ф" + ".txt");
  }
    public void РText()
  {
    File.WriteAllText(newText, "Р" + ".txt");
  }
    public void ЛText()
  {
    File.WriteAllText(newText, "Л" + ".txt");
  }
    public void ЫText()
  {
    File.WriteAllText(newText, "Ы" + ".txt");
  }
    public void КText()
  {
    File.WriteAllText(newText, "К" + ".txt");
  }
    public void ЩText()
  {
    File.WriteAllText(newText, "Щ" + ".txt");
  }
    public void ЮText()
  {
    File.WriteAllText(newText, "Ю" + ".txt");
  }
    public void ВText()
  {
    File.WriteAllText(newText, "В" + ".txt");
  }
    public void ЯText()
  {
    File.WriteAllText(newText, "Я" + ".txt");
  }
    public void ЦText()
  {
    File.WriteAllText(newText, "Ц" + ".txt");
  }
    public void ДText()
  {
    File.WriteAllText(newText, "Д" + ".txt");
  }
    public void ЗText()
  {
    File.WriteAllText(newText, "З" + ".txt");
  }
    public void ПText()
  {
    File.WriteAllText(newText, "П" + ".txt");
  }
    public void ЬText()
  {
    File.WriteAllText(newText, "Ь" + ".txt");
  }
      public void ЙText()
  {
    File.WriteAllText(newText, "Й" + ".txt");

  }
    public void БText()
  {
    File.WriteAllText(newText, "Б" + ".txt");
  }
    public void Play()
  {
      ColorBlock colors = changeColor.colors;
      colors.normalColor = Color.red; // устанавливаем цвет при нажатии
      changeColor.colors = colors;

      
  }
  public void Quit()
  {
    Application.Quit();
  }
  public void Tutorial()
  {
    SceneManager.LoadScene(2);
  }
  public void MapLevel()
  {
    SceneManager.LoadScene(2);
  }
}
