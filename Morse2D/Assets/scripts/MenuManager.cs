using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
  public void Play()
  {
    SceneManager.LoadScene(3);
  }
  public void Quit()
  {
    Application.Quit();
  }
  public void Tutorial()
  {
    SceneManager.LoadScene(2);
  }
  public void MapLevel()
  {
    SceneManager.LoadScene(2);
  }
}