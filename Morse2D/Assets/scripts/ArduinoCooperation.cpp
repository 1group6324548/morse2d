#include <LiquidCrystal.h>

LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

int lcdBrighPin = 11;
int switchPin = 13;
int lcdclr= 10;

int countOn = 0;
int countOff = 0;
int flagChar = 0;

int shortCodeTime = 3;    
int charEndTime = 8;    

int codes[10] = {0,0,0,0,0,0,0,0,0,0};
int codeIndex = 0;

void initCodes();
char getCodeToChar();
boolean buttonst = LOW;

void setup()
{
  Serial.begin(9600);
  
  pinMode(lcdBrighPin, OUTPUT);
  pinMode(switchPin, INPUT);
  pinMode(lcdclr, INPUT);
  
  lcd.begin(16, 2);
  
  analogWrite(lcdBrighPin, 60);
}

void loop()
{
  buttonst = digitalRead(lcdclr);
  if (buttonst == HIGH) {
    lcd.print("bye");
    lcd.clear();
  }
  int pressed = digitalRead(switchPin);
 
  if( pressed == 1 )
  {
   flagChar = 1;
    countOn++;
    countOff = 0;
  }
  else if ( flagChar == 1 )    
  {
    if( countOn > 0 )
    {
      if ( countOn < shortCodeTime )
      {
        codes[codeIndex] = 1;
        codeIndex++;
      }
      else 
      {
        codes[codeIndex] = 2;
        codeIndex++;
      }
      countOn = 0;
    }
    
    if( countOff > charEndTime )
    {
      flagChar = 0;
      char codeChar = getCodeToChar();
      lcd.print(codeChar);
      Serial.println(codeChar);
      initCodes();
    }
    else 
    {
      countOff++;
    }
  }
  delay(100);
}

void initCodes()
{
  int i = 0;
  for(i = 0; i < 10; i++)
  {
    codes[i] = 0;
  }
  codeIndex = 0;
}
char getCodeToChar()
{
  // A
  if ( codes[0] == 1 && codes[1] == 2 && codes[2] == 0 )
  {
    return 'A';
  } 

  // B
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 1 && codes[3] == 1 && codes[4] == 0)
  {
    return 'B';
  } 

  // C
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 2 && codes[3] == 1 && codes[4] == 0)
  {
    return 'C';
  } 

  // D
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 1 && codes[3] == 0 )
  {
    return 'D';
  } 

  // E
  if ( codes[0] == 1 && codes[1] == 0 )
  {
    return 'E';
  } 

  // F
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 2 && codes[3] == 1 && codes[4] == 0)
  {
    return 'F';
  } 

  // G
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 1 && codes[3] == 0)
  {
    return 'G';
  } 

  // H
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 1 && codes[3] == 1 && codes[4] == 0)
  {
    return 'H';
  } 

  // I
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 0 )
  {
    return 'I';
  } 

  // J
  if ( codes[0] == 1 && codes[1] == 2 && codes[2] == 2 && codes[3] == 2 && codes[4] == 0)
  {
    return 'J';
  } 

  // K
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 2 && codes[3] == 0 )
  {
    return 'K';
  } 

  // L
  if ( codes[0] == 1 && codes[1] == 2 && codes[2] == 1 && codes[3] == 1 && codes[4] == 0)
  {
    return 'L';
  } 

  // M
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 0 )
  {
    return 'M';
  } 

  // N
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 0)
  {
    return 'N';
  } 

  // O
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 2 && codes[3] == 0)
  {
    return 'O';
  } 

  // P
  if ( codes[0] == 1 && codes[1] == 2 && codes[2] == 2 && codes[3] == 1 && codes[4] == 0)
  {
    return 'P';
  } 

  // Q
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 1 && codes[3] == 2 && codes[4] == 0)
  {
    return 'Q';
  } 

  // R
  if ( codes[0] == 1 && codes[1] == 2 && codes[2] == 1 && codes[3] == 0)
  {
    return 'R';
  } 

  // S
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 1 && codes[3] == 0)
  {
    return 'S';
  } 

  // T
  if ( codes[0] == 2 && codes[1] == 0 )
  {
    return 'T';
  } 

  // U
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 2 && codes[3] == 0)
  {
    return 'U';
  } 

  // V
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 1 && codes[3] == 2 && codes[4] == 0)
  {
    return 'V';
  } 

  // W
  if ( codes[0] == 1 && codes[1] == 2 && codes[2] == 2 && codes[3] == 0)
  {
    return 'W';
  } 

  // X
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 1 && codes[3] == 2 && codes[4] == 0)
  {
    return 'X';
  }

Лев, [28.11.2023 18:14]
// Y
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 2 && codes[3] == 2 && codes[4] == 0)
  {
    return 'Y';
  } 

  // Z
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 1 && codes[3] == 1 && codes[4] == 0)
  {
    return 'Z';
  } 

  // 1
  if ( codes[0] == 1 && codes[1] == 2 && codes[2] == 2 && codes[3] == 2 && codes[4] == 2 && codes[5] == 0)
  {
    return '1';
  } 

  // 2
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 2 && codes[3] == 2 && codes[4] == 2 && codes[5] == 0)
  {
    return '2';
  } 

  // 3
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 1 && codes[3] == 2 && codes[4] == 2 && codes[5] == 0)
  {
    return '3';
  } 

  // 4
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 1 && codes[3] == 1 && codes[4] == 2 && codes[5] == 0)
  {
    return '4';
  } 

  // 5
  if ( codes[0] == 1 && codes[1] == 1 && codes[2] == 1 && codes[3] == 1 && codes[4] == 1 && codes[5] == 0)
  {
    return '5';
  } 

  // 6
  if ( codes[0] == 2 && codes[1] == 1 && codes[2] == 1 && codes[3] == 1 && codes[4] == 1 && codes[5] == 0)
  {
    return '6';
  } 

  // 7
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 1 && codes[3] == 1 && codes[4] == 1 && codes[5] == 0)
  {
    return '7';
  } 

  // 8
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 2 && codes[3] == 1 && codes[4] == 1 && codes[5] == 0)
  {
    return '8';
  } 

  // 9
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 2 && codes[3] == 2 && codes[4] == 1 && codes[5] == 0)
  {
    return '9';
  } 

  // 0
  if ( codes[0] == 2 && codes[1] == 2 && codes[2] == 2 && codes[3] == 2 && codes[4] == 2 && codes[5] == 0)
  {
    return '0';
  } 


  return ' ';
}