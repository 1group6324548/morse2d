using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour
{
	public AudioSource soundPlay;
	private bool isPlay = false;
	public void PlayThisSoundEffect()
	{
		isPlay = !isPlay;
		if(isPlay)
		{
			soundPlay.Play();
		}
		else
		{
			soundPlay.Stop();	
		}
	}
}
