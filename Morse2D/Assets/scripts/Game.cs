using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Game : MonoBehaviour
{
private int sec = 0;
private int min = 0;
private TMP_Text _GameTimer;
private bool pressBut = false;
private int delta = 1;

    // Start is called before the first frame update
    void Start()
    {
        _GameTimer = GameObject.Find("GameTimer").GetComponent<TMP_Text>();
	StartCoroutine(ITimer());
    }

	IEnumerator ITimer() {
		while(true) {
			if (sec == 59) {
				min++;
				sec = -1;
			}
			sec += delta;
			_GameTimer.text = min.ToString("D2") + ":" + sec.ToString("D2");
			yield return new WaitForSeconds(1);
		}
	}
	public void OnButtonDown() {
		if (pressBut) {
			GameObject.Find("TipABC").transform.position = new Vector3(2200, 450, 0);
			pressBut = !pressBut;
		}
		else {
			GameObject.Find("TipABC").transform.position = new Vector3(1600, 450, 0);
			pressBut = !pressBut;
		}
	}
}
