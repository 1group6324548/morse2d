using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;
using Random=UnityEngine.Random;
using System;
using System.IO;

public class CheckWord : MonoBehaviour
{
   private string input;
   private Dictionary<char, string> morseCode;
   private Dictionary<string, string> keyWords;
   public TMPro.TextMeshProUGUI StartWord;
   public GameObject Win;
   public GameObject Lose;
   public TMPro.TextMeshProUGUI InputWord;
   public TMPro.TextMeshProUGUI OutputResult;
   public TMPro.TextMeshProUGUI Score;
   private int score;
   private int count = 0;
   private int costOneSymb = 10;
   private string mainword;
   private Stack<string> Words;
   private int priceForClue = 20;
   private bool isPressedLong = false;
   private bool isHoldingButton = false;
   public TMPro.TextMeshProUGUI Path;
  private float holdStartTime;
  private int countWords = 2;
  // меню
   public static bool GameIsPaused = false;
   public GameObject pauseMenuUI;

void Update()
{
    if (Input.GetKey(KeyCode.LeftShift))
    {
      
        if (!isHoldingButton)
        {
            isHoldingButton = true;
            holdStartTime = Time.time;
        }
        // Выполнить действие при удерживании кнопки

        if (Time.time - holdStartTime >= 0.3f) // Проверяем, удерживается ли кнопка
        {
            isPressedLong = true;
            isHoldingButton = false;
        }
    }
    else if (Input.GetKeyUp(KeyCode.LeftShift))
    {
      if(!isPressedLong)
        AddedSymb("·");
      else
       AddedSymb("-");
      isPressedLong = false;
    }
    else
        isHoldingButton = false;
}
   void Start()
   {
      var ReadPathFromFile = Application.dataPath + "/Resources/" + File.ReadAllLines(Application.dataPath + "/words.txt")[0];
      morseCode = new Dictionary<char, string>()
      {
         {'А',  "·-"}, {'Б', "-···" }, {'В',   "·--"}, {'Г',   "--·"}, {'Д', "-··" },
         {'Е',   "·"}, {'Ж',   "···-"}, {'З',   "--··"}, {'И',   "··"}, {'Й',   "·---"},
         {'К', "-·-"}, {'Л',    "·-··"}, {'М',  "--"}, {'Н', "-·"}, {'О',    "---"},
         {'П',  "·--·"}, {'Р',   "·-·"}, {'С',   "···"}, {'Т',   "-"}, {'У', "··-"},
         {'Ф',  "··-·"}, {'Х',  "····"}, {'Ц', "-·-·"}, {'Ч',   "---·"}, {'Ш',    "----"},
         {'Щ',  "--·-"}, {'Ъ',   "-··-"}, {'Ы', "-·--"}, {'Ь', "-··-"}, {'Э',  "··-··"},
         {'Ю',  "··--"}, {'Я',   "·-·-"}
      };
      keyWords = new Dictionary<string, string>(){
        {"--··----", "меню"}, // !м
        {"--··---·", "настройки"}, //!н
        {"--··-----", "обучение"}, // !о
        {"--··--", "подсказка"}, // ?
      };
      var words = File.ReadAllLines(ReadPathFromFile);
      List<int> repeatedwords = new List<int>();
      Words = new Stack<string>();
      int maxCountWord = words.Length;
      for(int i = 0; i < countWords; i++)
      {
        int idx = Random.Range(0, maxCountWord);
        words[idx] = words[idx].Replace(" ", "");
        if(!repeatedwords.Contains(idx))
        {
          Words.Push(words[idx]);
          repeatedwords.Add(idx);
        }
        else i--;
      }
      
      score = 0;
      NextWordStart();
   }
   private void NextWordStart()
   {
      if(Words.Count != 0)
      {
        string txt = Words.Pop();
        mainword = txt;
        string colorw = "";
        for (int i = 0; i < txt.Length; i++)
          colorw += i == 0 ? $"<color=#ff0000>{txt[i].ToString()}</color>" : txt[i].ToString();
        StartWord.text = colorw;
      }
      else 
      {
        Win.SetActive(true);
      }
   }
   private void CheckMorseCodeOnKeyWords()
   {
        if(keyWords[InputWord.text] == "меню")
        {
          SceneManager.LoadScene(0);
        }
        else if(keyWords[InputWord.text] == "настройки")
        {
          Pause();
           InputWord.text = "";
        }
        else if(keyWords[InputWord.text] == "обучение")
        {
          SceneManager.LoadScene(2);
        }
        else if(keyWords[InputWord.text] == "подсказка")
        {
          GiveClue();
        }
        return;
   }
   private string GetFullWordInMorse()
   {
      string fullcodeword = "";
      foreach(var x in mainword)
      {
        if (morseCode.ContainsKey(char.ToUpper(x)))
          fullcodeword += morseCode[char.ToUpper(x)];
      }
      return fullcodeword;
   }
   public void isCorrectWord()
   {
      if(keyWords.ContainsKey(InputWord.text))
      {
        CheckMorseCodeOnKeyWords();
        return;
      }

      string mainWordInMorse = GetFullWordInMorse();
      // если морзе код равен текущему слову полностью

      if(mainWordInMorse == InputWord.text)
      {
        score += 200;
        count = 0;
        NextWordStart();
        InputWord.text = "";

	//gif
      }
      else 
      {
        string outputword = "";
        for (int i = 0; i < count; i++)
        {
          outputword += $"<color=#008000>{mainword[i].ToString()}</color>";
        }
        string defaultWord = StartWord.text;
        int tmpcount = count;
        bool isRightAnswer = false;
        string checkCode = "";
        for(int i = count; i < mainword.Length; i++)
        {
          count += 1;
          char ch = char.ToUpper(mainword[i]);
          checkCode += morseCode[ch];
          if(checkCode == InputWord.text)
          {
            isRightAnswer = true;
            if(count >= mainword.Length)
            {
		//gif
              count = 0;
              NextWordStart();
              InputWord.text = "";
              return;
            }
            else
            {
              outputword += $"<color=#008000>{mainword[i++].ToString()}</color>";
              for(int j = i; j < mainword.Length; j++)
              {
                if(j == i)
                  outputword += $"<color=#ff0000>{mainword[j].ToString()}</color>";
                else
                  outputword += $"<color=#000000>{mainword[j].ToString()}</color>";
              }
            }
            InputWord.text = "";
            break;
          }
          else
            outputword += $"<color=#008000>{mainword[i].ToString()}</color>";
        }
        if(!isRightAnswer)
        {
	//gifwrong
          outputword = defaultWord;
          count = tmpcount;
          score -= costOneSymb;
        } else score += costOneSymb * count;
        Score.text = score.ToString();
        StartWord.text = outputword; 
      }
   }
   void HideText()
   {
      OutputResult.text = "";
   }
  public void GiveClue()
  {
    InputWord.text = morseCode[char.ToUpper(mainword[count])];;
    score -= priceForClue;
    Score.text = score.ToString();
  }
   private int maxLength = 28;
    public void MyClickSpace()
   {
     AddedSymb("-");
   }
   public void MyClickPoint()
   {
     AddedSymb("·");
   }
   private void AddedSymb(string symb)
   {
     if(InputWord.text.Length < maxLength)
      InputWord.text += symb;
   }
   public void DeleteLastChar()
   {
      if(InputWord.text.Length > 0)
      {
        InputWord.text = InputWord.text.Substring(0, InputWord.text.Length - 1);
      }
   }
   // меню 
   public void Resume()
   {
      pauseMenuUI.SetActive(false);
      Time.timeScale = 1f;
      GameIsPaused = false;
   }
   void Pause()
   {
      pauseMenuUI.SetActive(true);
      Time.timeScale = 0f;
      GameIsPaused = true;
   }
   public void LoadMenu()
   {
      Time.timeScale = 1f;
      SceneManager.LoadScene(0);
   }
      public void ReloadGame()
   {
        Scene currentScene = SceneManager.GetActiveScene(); // получаем текущую сцену
        SceneManager.LoadScene(currentScene.name); // перезагружаем текущую сцену
   }
}
