using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickMorseButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public TMPro.TextMeshProUGUI InputWord;
     private int maxLength = 28;
    public float holdTime = 0.3f; // Время удерживания кнопки
    private bool isHolding = false; // Флаг, показывающий, удерживается ли кнопка в данный момент
    private float holdTimer = 0f; // Таймер удерживания кнопки
    private bool isPoint = false;
	public AudioClip shortPressSound;
    public AudioClip longPressSound;
    private AudioSource audioSource;

    public void OnPointerDown(PointerEventData eventData)
    {
        isHolding = true; // Устанавливаем флаг, что кнопка удерживается
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isHolding = false; // Сбрасываем флаг, что кнопка удерживается
        holdTimer = 0f; // Сбрасываем таймер удерживания
        if(isPoint) {
          AddedSymb("·");
	audioSource.PlayOneShot(shortPressSound);
	}	
        else {
          AddedSymb("-");
	audioSource.PlayOneShot(longPressSound);
	}	
        isPoint = false;
    }

	void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
    }

    private void Update()
    {
        if (isHolding)
        {
            holdTimer += Time.deltaTime; // Увеличиваем таймер удерживания
            if (holdTimer >= holdTime)
            {
                // Выполняем действие при удержании кнопки в течение указанного времени
                isPoint = false;
                return;
            }
            isPoint = true;
        }
    }
   private void AddedSymb(string symb)
   {
     if(InputWord.text.Length < maxLength)
      InputWord.text += symb;
   }
}
