using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signal : MonoBehaviour
{
	public AudioClip shortPressSound;
    public AudioClip longPressSound;
    private AudioSource audioSource;
    private float pressTime;
    private bool isPressed;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
    }

    void Update()
    {
        if (isPressed)
        {
            pressTime += Time.deltaTime;
        }
    }

    public void OnPointerDown()
    {
        isPressed = true;
        pressTime = 0f;
    }

    public void OnPointerUp()
    {
        isPressed = false;
        if (pressTime < 0.2f) // Для определения быстрого нажатия
        {
            audioSource.PlayOneShot(shortPressSound);
        }
        else // Для определения долгого нажатия
        {
            audioSource.PlayOneShot(longPressSound);
        }
    }
}
